import styles from "./FormOrder.module.scss";
import Link from "next/link";
import Image from "next/image";
import Head from "next/head";
import { animateScroll as scroll } from "react-scroll";
import React, { useState, useEffect } from "react";

const FormOrder = (props) => {
  const [value, setValue] = useState("");
  const [value2, setValue2] = useState("");

  const arrCategories = [
    "Индивидуальная",
    "Деловая",
    "«Love story»",
    "Для всей семьи",
    "Fashion-фотосессия",
    "Свадебная",
    "Портретная",
  ];

  const arrStudios = [
    "Студия «Comfort»",
    "Студия «Gold»",
    "Студия «Premium»",
    "Студия «Luxury»",
  ];

  const optionsCategories = arrCategories.map((title, index) => {
    return (
      <option key={index} value={index} className={styles.options}>
        {title}
      </option>
    );
  });

  const optionsStudios = arrStudios.map((title, index) => {
    return (
      <option key={index} value2={index} className={styles.options}>
        {title}
      </option>
    );
  });

  useEffect(() => {
    scroll.scrollToTop();
  }, []);
  return (
    <>
      <Head>
        <title>Photojet | Заказ фотосессии</title>
        <meta name="title" content="Photojet"></meta>
      </Head>
      <div className="overlay">
        <div className={styles.container}>
          <section className={styles.content}>
            <div className={styles.titleIconCloseWrapper}>
              <h2 className={styles.title}>Заказ фотосессии</h2>
              <Image
                className={styles.closeIcon}
                onClick={props.onClose}
                src="/closeIcon.png"
                width={30}
                height={30}
                alt="icon close"
              ></Image>
            </div>
            <p className={styles.text}>
              Заполните форму и оператор выйдет на связь
              <br /> в ближайшее время
            </p>

            <form method="post" className={styles.inputsWrapper}>
              <input
                className={styles.inputName}
                type="text"
                placeholder="Имя"
              />
              <input
                className={styles.inputEmail}
                type="email"
                placeholder="Email"
              />
              <input
                className={styles.inputPhone}
                type="phone"
                placeholder="Телефон"
              />

              <input
                className={styles.inputCalendar}
                type="date"
                placeholder="Дата фотосессии"
              />

              <div className={styles.selectWrapper}>
                <select
                  className={styles.categories}
                  value={value}
                  onChange={(event) => setValue(event.target.value)}
                >
                  {optionsCategories}
                </select>
              </div>
              <div className={styles.selectWrapper}>
                <select
                  className={styles.studios}
                  value2={value2}
                  onChange={(event) => setValue2(event.target.value)}
                >
                  {optionsStudios}
                </select>
              </div>
            </form>

            <div className={styles.map}>
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d17963.022529431993!2d37.61540526962256!3d55.751938740536076!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2snl!4v1678936639386!5m2!1sru!2snl"
                width="466"
                height="466"
                loading="lazy"
                referrerpolicy="no-referrer-when-downgrade"
              ></iframe>
            </div>
            <div className={styles.checkboxWrapper}>
              <input
                type="checkbox"
                className={styles.custom_checkbox}
                id="policy"
                value="yes"
              />
              <label className={styles.checkboxSize} for="policy"></label>

              <span className={styles.textPolicy}>
                Нажимая на кнопку, я даю согласие на обработку персональных
                данных и соглашаюсь c{" "}
                <Link href="/policy" legacyBehavior>
                  <a className={styles.underline}>
                    пользовательским соглашением и политикой конфиденциальности
                  </a>
                </Link>
              </span>
            </div>
            <input
              className={styles.orderButton}
              type="submit"
              value="ОТПРАВИТЬ ЗАЯВКУ"
              onClick={props.onClose}
            />
          </section>
        </div>
      </div>
    </>
  );
};
export default FormOrder;
