import Link from "next/link";
import styles from "./Footer.module.scss";
import Image from "next/image";

const Footer = () => {
  return (
    <footer className={styles.container}>
      <section className={styles.content}>
        <span className={styles.logo}>
          <Link href="/" legacyBehavior>
            <a>Photojet</a>
          </Link>
        </span>
        <nav className={styles.navigation}>
          <span className={styles.firstLineNavigation}>
            <Link href="/faq" legacyBehavior>
              <a>Частые вопросы</a>
            </Link>
            <Link href="/how-the-service-works" legacyBehavior>
              <a>Как работает сервис</a>
            </Link>
            <Link href="/service-cancellation" legacyBehavior>
              <a>Отказ от услуги</a>
            </Link>
            <Link href="/payment" legacyBehavior>
              <a>Оплата</a>
            </Link>
          </span>
          <span className={styles.secondLineNavigation}>
            <Link href="/contacts" legacyBehavior>
              <a>Контакты</a>
            </Link>
            <Link href="/feedback" legacyBehavior>
              <a>Обратная связь</a>
            </Link>
            <Link href="/recruiting" legacyBehavior>
              <a>Работайте с нами</a>
            </Link>
          </span>
          <div className={styles.thirdLineNavigation}>
            <Link href="/policy" legacyBehavior>
              <a className={styles.offer}>Политика обработки данных и оферта</a>
            </Link>

            <article className={styles.paymentsLogo}>
              <Image
                src="/paymentsLogo/mastercard.png"
                width={40}
                height={24}
                alt="mastercard icon"
              />
              <Image
                src="/paymentsLogo/visa.png"
                width={62}
                height={21}
                alt="visa icon"
              />
              <Image
                src="/paymentsLogo/mir.png"
                width={73}
                height={20}
                alt="mir icon"
              />
            </article>
          </div>
        </nav>
      </section>
    </footer>
  );
};

export default Footer;
