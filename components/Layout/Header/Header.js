import Link from "next/link";
import styles from "./Header.module.scss";
import Router from "next/router";
import React, { useState } from "react";

const router = Router;

const Header = () => {
  const miniScroll = () => {
    setTimeout(
      () => {
        if (window.innerWidth > 1000) {
          window.scrollTo({
            top: 1150,
            behavior: "smooth",
          });
        } else if (window.innerWidth > 600 && window.innerWidth < 1000) {
          window.scrollTo({
            top: 2050,
            behavior: "smooth",
          });
        } else if (window.innerWidth < 600) {
          window.scrollTo({
            top: 1350,
            behavior: "smooth",
          });
        }
      },

      350
    );
  };

  const [burgerClass, setBurgerClass] = useState("burgerBar unclicked");
  const [menuClass, setMenuClass] = useState("menu hidden");
  const [isMenuClicked, setIsMenuClicked] = useState(false);

  const updateMenu = () => {
    if (!isMenuClicked) {
      setBurgerClass("burgerBar clicked");
      setMenuClass("menu visible");
    } else {
      setBurgerClass("burgerBar unclicked");
      setMenuClass("menu hidden");
    }
    setIsMenuClicked(!isMenuClicked);
  };

  return (
    <div className={styles.container}>
      <section className={styles.content}>
        <span className={styles.logo}>
          <Link href="/" legacyBehavior>
            <a>Photojet</a>
          </Link>
        </span>
        <ul className={styles.navigation_list}>
          <li
            onClick={() => {
              localStorage.setItem("individual", "individual");
              router.push("/");
              miniScroll();
            }}
          >
            Индивидуальная
          </li>
          <li
            onClick={() => {
              localStorage.setItem("business", "business");
              router.push("/");
              miniScroll();
            }}
          >
            Деловая
          </li>
          <li
            onClick={() => {
              localStorage.setItem("lovestory", "lovestory");
              router.push("/");
              miniScroll();
            }}
          >
            «Love story»
          </li>
          <li
            onClick={() => {
              localStorage.setItem("family", "family");
              router.push("/");
              miniScroll();
            }}
          >
            Для всей семьи
          </li>
          <li
            onClick={() => {
              localStorage.setItem("fashion", "fashion");
              router.push("/");
              miniScroll();
            }}
          >
            Fashion-фотосессия
          </li>
          <li
            onClick={() => {
              localStorage.setItem("wedding", "wedding");
              router.push("/");
              miniScroll();
            }}
          >
            Свадебная
          </li>
          <li
            onClick={() => {
              localStorage.setItem("portrait", "portrait");
              router.push("/");
              miniScroll();
            }}
          >
            Портретная
          </li>
        </ul>

        <article className="burgerMenuWrapper" onClick={updateMenu}>
          <div className="burgerMenuContainer">
            <div className="burgerMenuCircle">
              <div className="burgerMenu">
                <div className={burgerClass}></div>
                <div className={burgerClass}></div>
                <div className={burgerClass}></div>
              </div>
            </div>
          </div>
        </article>
      </section>

      <div className={menuClass}>
        <ul className={styles.navigationListMenu}>
          <li
            onClick={() => {
              localStorage.setItem("individual", "individual");
              router.push("/");
              miniScroll();
              setBurgerClass("burgerBar unclicked");
              setMenuClass("menu hidden");
            }}
          >
            Индивидуальная
          </li>
          <li
            onClick={() => {
              localStorage.setItem("business", "business");
              router.push("/");
              miniScroll();
              setBurgerClass("burgerBar unclicked");
              setMenuClass("menu hidden");
            }}
          >
            Деловая
          </li>
          <li
            onClick={() => {
              localStorage.setItem("lovestory", "lovestory");
              router.push("/");
              miniScroll();
              setBurgerClass("burgerBar unclicked");
              setMenuClass("menu hidden");
            }}
          >
            «Love story»
          </li>
          <li
            onClick={() => {
              localStorage.setItem("family", "family");
              router.push("/");
              miniScroll();
              setBurgerClass("burgerBar unclicked");
              setMenuClass("menu hidden");
            }}
          >
            Для всей семьи
          </li>
          <li
            onClick={() => {
              localStorage.setItem("fashion", "fashion");
              router.push("/");
              miniScroll();
              setBurgerClass("burgerBar unclicked");
              setMenuClass("menu hidden");
            }}
          >
            Fashion-фотосессия
          </li>
          <li
            onClick={() => {
              localStorage.setItem("wedding", "wedding");
              router.push("/");
              miniScroll();
              setBurgerClass("burgerBar unclicked");
              setMenuClass("menu hidden");
            }}
          >
            Свадебная
          </li>
          <li
            onClick={() => {
              localStorage.setItem("portrait", "portrait");
              router.push("/");
              miniScroll();
              setBurgerClass("burgerBar unclicked");
              setMenuClass("menu hidden");
            }}
          >
            Портретная
          </li>
        </ul>
      </div>
    </div>
  );
};
export default Header;
