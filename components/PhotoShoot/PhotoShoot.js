import Image from "next/image";
import styles from "./PhotoShoot.module.scss";
import React, { useState, useEffect } from "react";

const PhotoShoot = (props) => {
  const arr = [
    {
      id: "individual",
      title: "Индивидуальная",
      text: "Индивидуальная фотосессия от Photojet - это возможность стать главным героем собственной фотоистории. Мы создаем уникальные образы и настраиваем атмосферу, которая поможет Вам раскрыть свою индивидуальность и передать все те эмоции, которые Вы хотели бы сохранить на фото. Команда профессиональных фотографов и стилистов работает c каждым клиентом персонально, чтобы создать наиболее подходящий образ, который выразит Вашу уникальность и индивидуальность.",

      firstPicUrl: "/sliderCollage/1-1pic.png",
      secondPicUrl: "/sliderCollage/1-2pic.png",
      tallPicUrl: "/sliderCollage/1-Tallpic.png",
      widePicUrl: "/sliderCollage/1-Widepic.png",
    },

    {
      id: "business",
      title: "Деловая",
      text: "Деловая фотосессия – это возможность передать свою профессиональность и уверенность на фото. Команда профессиональных фотографов Photojet поможет Вам создать образ, который будет соответствовать Вашим требованиям и позволит Выглядеть на фотографиях на все сто процентов. Доверьте нам создание Ваших профессиональных фото, а мы гарантируем, что результат нашей работы поможет Вам поднять свой профессиональный имидж и сделать первое впечатление незабываемым.",

      firstPicUrl: "/sliderCollage/2-1pic.png",
      secondPicUrl: "/sliderCollage/2-2pic.png",
      tallPicUrl: "/sliderCollage/2-Tallpic.png",
      widePicUrl: "/sliderCollage/2-Widepic.png",
    },

    {
      id: "lovestory",
      title: "«Love story»",
      text: "Фотосессия в стиле «Love story» – это возможность запечатлеть Вашу любовь и создать красивые воспоминания, которые будут с Вами на всю жизнь. Команда профессиональных фотографов Photojet поможет Вам создать атмосферу, в которой Вы сможете полностью раскрыться и передать на фото всю ту нежность и любовь, которую Вы испытываете друг к другу. Мы гарантируем, что результат нашей работы станет незабываемым воспоминанием и поможет сохранить Вашу любовь на всю жизнь. Доверьте Photojet создание Вашей «Love story» фотосессии и она станет настоящим произведением искусства.",

      firstPicUrl: "/sliderCollage/3-1pic.png",
      secondPicUrl: "/sliderCollage/3-2pic.png",
      tallPicUrl: "/sliderCollage/3-Tallpic.png",
      widePicUrl: "/sliderCollage/3-Widepic.png",
    },

    {
      id: "family",
      title: "Для всей семьи",
      text: "Семейная фотосессия – это не только возможность запечатлеть Вашу семью, но и создать красивые воспоминания, которые Вы сможете подарить друг другу в будущем. Поэтому команда Photojet старается создать индивидуальный подход к каждому клиенту и помочь выбрать тот образ, который будет идеально соответствовать Вашей семейной истории. Мы предлагаем широкий спектр услуг: от консультации и подбора места для фотосессии, до обработки фотографий и создания красивого альбома. Профессиональные фотографы Photojet владеют техниками, которые помогут подчеркнуть семейную связь и сделать фотосессию незабываемым подарком для Вашей семьи.",

      firstPicUrl: "/sliderCollage/4-1pic.png",
      secondPicUrl: "/sliderCollage/4-2pic.png",
      tallPicUrl: "/sliderCollage/4-Tallpic.png",
      widePicUrl: "/sliderCollage/4-Widepic.png",
    },

    {
      id: "fashion",
      title: "Fashion-фотосессия",
      text: "Photojet предлагает услуги fashion-фотосъемки для журналов и изданий. Модельная фотосъемка – это возможность создать уникальный контент, который будет привлекать внимание читателей и зрителей. Команда профессиональных фотографов Photojet готова предложить высококачественные услуги фотосъемки для различных журналов и изданий. Мы понимаем, что фотографии являются ключевым элементом, который привлекает внимание к изданию, поэтому наши специалисты используют весь свой опыт и знания, чтобы создать фотосессию, которая подчеркнет личность героя, тему материала и привлечет читателей.",

      firstPicUrl: "/sliderCollage/5-1pic.png",
      secondPicUrl: "/sliderCollage/5-2pic.png",
      tallPicUrl: "/sliderCollage/5-Tallpic.png",
      widePicUrl: "/sliderCollage/5-Widepic.png",
    },

    {
      id: "wedding",

      title: "Свадебная",
      text: "Свадебная фотосессия – это не только важный элемент свадебного дня, но и воспоминание на всю жизнь, которое сохранит эмоции и чувства этого дня. Мы предлагаем профессиональные услуги свадебной фотосъемки, которые позволят Вам сохранить самые яркие моменты этого волшебного дня в неповторимых кадрах. Команда профессиональных фотографов Photojet знает, как создать неповторимую атмосферу на фотосессии и подчеркнуть Вашу индивидуальность и красоту в самый важный день в жизни. Мы готовы работать в любых условиях и местах, чтобы создать для Вас идеальную свадебную фотосессию.",

      firstPicUrl: "/sliderCollage/6-1pic.png",
      secondPicUrl: "/sliderCollage/6-2pic.png",
      tallPicUrl: "/sliderCollage/6-Tallpic.png",
      widePicUrl: "/sliderCollage/6-Widepic.png",
    },

    {
      id: "portrait",
      title: " Портретная",
      text: "Позвольте нам создать для вас незабываемую портретную фотосессию, которая запечатлит вашу красоту и индивидуальность.Профессиональные фотографы создадут для вас атмосферу уюта и комфорта, чтобы вы могли полностью расслабиться и насладиться процессом фотосъемки. Мы знаем, что каждый человек уникален и имеет свой неповторимый стиль, поэтому будем работать в тесном сотрудничестве с вами, чтобы выразить вашу личность через фотографии.Не упустите возможность создать красивые и эмоциональные портреты, которые останутся с вами на всю жизнь. Забронируйте свою портретную фотосессию прямо сейчас!",

      firstPicUrl: "/sliderCollage/7-1pic.png",
      secondPicUrl: "/sliderCollage/7-2pic.png",
      tallPicUrl: "/sliderCollage/7-Tallpic.png",
      widePicUrl: "/sliderCollage/7-Widepic.png",
    },
  ];

  const [show, setShow] = useState("individual");

  const toggle = (index) => {
    setShow(index);
  };

  useEffect(() => {
    const fromLocalStorage1 = localStorage.getItem("individual");
    const fromLocalStorage2 = localStorage.getItem("business");
    const fromLocalStorage3 = localStorage.getItem("lovestory");
    const fromLocalStorage4 = localStorage.getItem("family");
    const fromLocalStorage5 = localStorage.getItem("fashion");
    const fromLocalStorage6 = localStorage.getItem("wedding");
    const fromLocalStorage7 = localStorage.getItem("portrait");
    if (fromLocalStorage1) {
      setShow(fromLocalStorage1);
      localStorage.clear();
    }
    if (fromLocalStorage2) {
      setShow(fromLocalStorage2);
      localStorage.clear();
    }
    if (fromLocalStorage3) {
      setShow(fromLocalStorage3);
      localStorage.clear();
    }
    if (fromLocalStorage4) {
      setShow(fromLocalStorage4);
      localStorage.clear();
    }
    if (fromLocalStorage5) {
      setShow(fromLocalStorage5);
      localStorage.clear();
    }
    if (fromLocalStorage6) {
      setShow(fromLocalStorage6);
      localStorage.clear();
    }
    if (fromLocalStorage7) {
      setShow(fromLocalStorage7);
      localStorage.clear();
    }
  });

  return (
    <div className={styles.container}>
      <section className={styles.content}>
        {arr.map((item) =>
          show === item.id ? (
            <div className={styles.mapWrapper} key={item.id}>
              <article className={styles.titleTextWrapper}>
                <nav className={styles.navigationWrapper}>
                  {show === item.id &&
                    arr.map((item) =>
                      show === item.id ? (
                        <div
                          className={styles.titleWrapper}
                          key={item.id}
                          onClick={() => toggle(item.id)}
                        >
                          <span className={styles.title}>{item.title}</span>
                          <span className={styles.titleBorderActive}></span>
                        </div>
                      ) : (
                        <div
                          className={styles.titleWrapper}
                          key={item.id}
                          onClick={() => toggle(item.id)}
                        >
                          <span className={styles.title}>{item.title}</span>
                          <span className={styles.titleBorder}></span>
                        </div>
                      )
                    )}
                </nav>
                <div className={styles.textWrapper}>
                  <span className={styles.text}>{item.text}</span>
                </div>
              </article>
              {show === item.id && (
                <article className={styles.collageBtnWrapper} key={item.id}>
                  <div className={styles.collage}>
                    <Image
                      className={styles.firstPic}
                      src={item.firstPicUrl}
                      width={408}
                      height={308}
                      alt={`Образец фотосессии из категории ${item.title}`}
                    ></Image>
                    <Image
                      className={styles.secondPic}
                      src={item.secondPicUrl}
                      width={408}
                      height={308}
                      alt={`Образец фотосессии из категории ${item.title}`}
                    ></Image>
                    <Image
                      className={styles.tallPic}
                      src={item.tallPicUrl}
                      width={408}
                      height={632}
                      alt={`Образец фотосессии из категории ${item.title}`}
                    ></Image>
                    <Image
                      className={styles.widePic}
                      src={item.widePicUrl}
                      width={832}
                      height={308}
                      alt={`Образец фотосессии из категории ${item.title}`}
                    ></Image>
                  </div>

                  <button onClick={props.onClickForm} className={styles.btn}>
                    ЗАКАЗАТЬ
                  </button>
                </article>
              )}
            </div>
          ) : (
            <div key={item.id}></div>
          )
        )}
      </section>
    </div>
  );
};
export default PhotoShoot;
