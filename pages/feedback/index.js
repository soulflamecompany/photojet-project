import styles from "./Feedback.module.scss";
import Link from "next/link";
import Head from "next/head";

const Feedback = () => {
  return (
    <>
      <Head>
        <title>Photojet | Обратная связь</title>
        <meta name="title" content="Pjotojet"></meta>
      </Head>
      <div className={styles.container}>
        <section className={styles.content}>
          <nav className={styles.navBar}>
            <Link href="/faq" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Часто задаваемые вопросы</a>
              </span>
            </Link>
            <Link href="/how-the-service-works" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Как работает сервис</a>
              </span>
            </Link>
            <Link href="/service-cancellation" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Отказ от услуги</a>
              </span>
            </Link>
            <Link href="/payment" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Оплата</a>
              </span>
            </Link>
            <Link href="/contacts" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Контакты</a>
              </span>
            </Link>
            <Link href="/feedback" legacyBehavior>
              <span className={styles.navTitleActive}>
                <a>Обратная связь</a>
              </span>
            </Link>
            <Link href="/recruiting" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Работайте с нами</a>
              </span>
            </Link>
          </nav>

          <article className={styles.mainContent}>
            <h1 className={styles.title}>Напишите нам</h1>
            <form method="post" className={styles.inputsWrapper}>
              <input
                type="text"
                placeholder="Имя"
                className={styles.inputName}
              />
              <input
                type="email"
                className={styles.inputEmail}
                placeholder="Email"
              />
              <textarea
                rows="10"
                cols="25"
                placeholder="Сообщение"
                className={styles.inputTextArea}
              ></textarea>
              <input
                className={styles.orderButton}
                type="submit"
                value="Отправить сообщение"
              />
            </form>
          </article>
        </section>
      </div>
    </>
  );
};

export default Feedback;
