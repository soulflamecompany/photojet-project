import styles from "./Recruiting.module.scss";
import Link from "next/link";
import Head from "next/head";

const Recruiting = () => {
  return (
    <>
      <Head>
        <title>Photojet | Работайте с нами</title>
        <meta name="title" content="Pjotojet"></meta>
      </Head>
      <div className={styles.container}>
        <section className={styles.content}>
          <nav className={styles.navBar}>
            <Link href="/faq" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Часто задаваемые вопросы</a>
              </span>
            </Link>
            <Link href="/how-the-service-works" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Как работает сервис</a>
              </span>
            </Link>
            <Link href="/service-cancellation" legacyBehavior>
              <span className={styles.navTitle}>
                {" "}
                <a>Отказ от услуги</a>
              </span>
            </Link>
            <Link href="/payment" legacyBehavior>
              <span className={styles.navTitle}>
                {" "}
                <a>Оплата</a>
              </span>
            </Link>
            <Link href="/contacts" legacyBehavior>
              <span className={styles.navTitle}>
                {" "}
                <a>Контакты</a>
              </span>
            </Link>
            <Link href="/feedback" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Обратная связь</a>
              </span>
            </Link>
            <Link href="/recruiting" legacyBehavior>
              <span className={styles.navTitleActive}>
                <a>Работайте с нами</a>
              </span>
            </Link>
          </nav>

          <article className={styles.mainContent}>
            <h1 className={styles.title}>
              Запечатлите свои лучшие моменты с Photojet!
            </h1>
            <span className={styles.tableTitle}>Стоимость аренды студий</span>
            <table className={styles.table}>
              <tr>
                <td className={styles.tableString}>Студия «Comfort»</td>
                <td className={styles.tableString}>от 1 800 рублей/час</td>
              </tr>
              <tr>
                <td className={styles.tableString}>Студия «Gold»</td>
                <td className={styles.tableString}>от 3 900 рублей/час</td>
              </tr>
              <tr>
                <td className={styles.tableString}>Студия «Premium»</td>
                <td className={styles.tableString}>от 6 600 рублей/час</td>
              </tr>
              <tr>
                <td className={styles.tableString}>Студия «Luxury»</td>
                <td className={styles.tableString}>от 14 000 рублей/час</td>
              </tr>
            </table>
            <span className={styles.tableTitle}>
              Стоимость работы фотографов
            </span>
            <table className={styles.table}>
              <tr>
                <td className={styles.tableString}>Фотограф мастер</td>
                <td className={styles.tableString}>от 2 700 рублей/час</td>
              </tr>
              <tr>
                <td className={styles.tableString}>Фотограф топ-мастер</td>
                <td className={styles.tableString}>от 6 900 рублей/час</td>
              </tr>
              <tr>
                <td className={styles.tableString}>Фотограф арт-директор</td>
                <td className={styles.tableString}>от 15 000 рублей/час</td>
              </tr>
            </table>
            <div className={styles.textWrapper}>
              <span className={styles.text}>
                Конечная стоимость услуг зависит от выбранных вами параметров
                фотосессии и может варьироваться в зависимости от индивидуальных
                потребностей и желаний. Мы предлагаем различные пакеты, начиная
                от базовых до расширенных, с разными возможностями и
                продолжительностью фотосессии.
              </span>
              <span className={styles.text}>
                Для получения подробной информации о стоимости наших услуг,
                пожалуйста, заполните форму на сайте, и мы свяжемся с вами в
                ближайшее время. Мы готовы предложить индивидуальный подход и
                подобрать пакет, который наилучшим образом подходит именно вам.
              </span>
            </div>
          </article>
        </section>
      </div>
    </>
  );
};

export default Recruiting;
