import styles from "./ServiceCancellation.module.scss";
import Link from "next/link";
import Head from "next/head";

const ServiceCancellation = () => {
  return (
    <>
      <Head>
        <title>Photojet | Отказ от услуг</title>
        <meta name="title" content="Pjotojet"></meta>
      </Head>
      <div className={styles.container}>
        <section className={styles.content}>
          <nav className={styles.navBar}>
            <Link href="/faq" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Часто задаваемые вопросы</a>
              </span>
            </Link>
            <Link href="/how-the-service-works" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Как работает сервис</a>
              </span>
            </Link>
            <Link href="/service-cancellation" legacyBehavior>
              <span className={styles.navTitleActive}>
                <a>Отказ от услуги</a>
              </span>
            </Link>
            <Link href="/payment" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Оплата</a>
              </span>
            </Link>
            <Link href="/contacts" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Контакты</a>
              </span>
            </Link>
            <Link href="/feedback" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Обратная связь</a>
              </span>
            </Link>
            <Link href="/recruiting" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Работайте с нами</a>
              </span>
            </Link>
          </nav>

          <article className={styles.mainContent}>
            <h1 className={styles.title}>
              Правила возврата средств при отказе от услуги
            </h1>
            <ol className={styles.textWrapper}>
              <span className={styles.textTitle}>
                Право потребителя на расторжение договора об оказании услуги
                регламентируется статьей 32 федерального закона «О защите прав
                потребителей».
              </span>
              <li className={styles.text}>
                Потребитель вправе расторгнуть договор об оказании услуги в
                любое время, уплатив исполнителю часть цены пропорционально
                части оказанной услуги до получения извещения о расторжении
                указанного договора и возместив исполнителю расходы,
                произведенные им до этого момента в целях исполнения договора,
                если они не входят в указанную часть цены услуги.
              </li>
              <li className={styles.text}>
                Потребитель при обнаружении недостатков оказанной услуги вправе
                по своему выбору потребовать:
              </li>
              <span className={styles.text}>
                а{")"} безвозмездного устранения недостатков;
              </span>
              <span className={styles.text}>
                б{")"} соответствующего уменьшения цены;
              </span>
              <span className={styles.text}>
                в{")"} возмещения понесенных им расходов по устранению
                недостатков своими силами или третьими лицами.
              </span>
              <li className={styles.text}>
                Потребитель вправе предъявлять требования, связанные с
                недостатками оказанной услуги, если они обнаружены в течение
                гарантийного срока, а при его отсутствии в разумный срок, в
                пределах двух лет со дня принятия оказанной услуги.
              </li>
              <li className={styles.text}>
                Исполнитель отвечает за недостатки услуги, на которую не
                установлен гарантийный срок, если потребитель докажет, что они
                возникли до ее принятия им или по причинам, возникшим до этого
                момента.
              </li>
            </ol>
          </article>
        </section>
      </div>
    </>
  );
};
export default ServiceCancellation;
